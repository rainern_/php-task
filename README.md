# PHP Tehtävä Dokumentaatio

## Table of Contents
- Huomiot
- Kansioiden sisältö
- Aikataulu
  - Päivittäinen ajankäyttö
- Tausta
  - Tehtävät asiat
  - Havainnot
- Environment
- Esisunnittelu
  - Toiminta
  - Näkymä
  - Database
  - Mahdolliset ongelmat
- Suunitelma/Dokumentit
  - Toimintatapa
  - Database
  - Website Rakenne
  - Use-case
  - Sequence diagram
- Toteutus
  - Database
  - form.php
  - submitprocess.php
- Ongelmat
  - PHP ei tunnistanut php PostgreSQL osion komentoja
- Loppusanat | Lopputulos

---
## Huomiot
-  Tämä kyseinen README tiedosto ei sisällä asennusten läpikäyntiä. Tämä löytyy install kansiosta.
- Tämä README sisältää suurimaksi osaksi suunittelua ja toiminallista step by step toteutusta. Tästä syystä css osuutta ei ole.
- Tämä dokumentti on jo tarpeeksi pitkä --> Code Review osuus on code-review kansiossa, joka myös käy läpi css osan.

---
## Kansioiden sisältö
code-review: Sisältää README dokumentin jossa käydään käytössä oleva koodi läpi.

concept-files: Sisältää suunitelma .drawio tiedostoja.

images: Nimensä mukaan pitää kaikki dokumentti kuvat.

install-instructions: README tiedosto, joka sisältää ohjelmiston asennus vaiheet step by step.

project: Sisältää itse projektin ja kaiken siihen tarpeellisen.

---
## Aikataulu/Aikaa käytetty
    Aihe: Tausta Osuus              Time: 11.00-11.20     Date: 16.02.2021      Session: 1
    Aihe: PHP Asunnus               Time: 12.45-12.55     Date: 16.02.2021      Session: 2
    Aihe: PHP Havainnot             Time: 13.20-13.40     Date: 16.02.2021      Session: 2
    Aihe: Esisuunnittelu + Doc      Time: 15.25-17.25     Date: 16.02.2021      Session: 3
    Aihe: Doc + php test & install  Time: 7.50-9.00       Date: 17.02.2021      Session: 1
    Aihe: Doc + database setup      Time: 9.00-11.00      Date: 17.02.2021      Session: 1
    Aihe: Doc + php dev             Time: 14.30-16.00     Date: 17.02.2021      Session: 2
    Aihe: PHP extension problem     Time: 16.00-18.00     Date: 17.02.2021      Session: 2
    Aihe: Problem dokumentointi     Time: 18.00-19.25     Date: 17.02.2021      Session: 2
    Aihe: Transfer data to db + Doc Time: 5.30-7.30       Date: 18.02.2021      Session: 1
    Aihe: Dokumentin Siivous        Time: 7.30-8.00       Date: 18.02.2021      Session: 1
    Aihe: Concept piirruksien teko  Time: 10.15-11.00     Date: 18.02.2021      Session: 2
    Aihe: Form sivuston kaunistelu  Time: 12.00-14.00     Date: 18.02.2021      Session: 3
    Aihe: Logon ja faviconin teko   Time: 14.00-14.45     Date: 18.02.2021      Session: 3
    Aihe: Add favi & logo + tweaks  Time: 14.45-15.30     Date: 18.02.2021      Session: 3
    Aihe: Submit sivun kaunistelu   Time: 15.30-16.30     Date: 18.02.2021      Session: 3
    Aihe: Loppuosuus dokumentointi  Time: 16.30-17.30     Date: 18.02.2021      Session: 3
    Aihe: Code Review kirjoitus     Time: 6.00-9.00       Date: 19.02.2021      Session: 1
    Aihe: Use-case & sequence doc   Time: 9.45-10.15      Date: 19.02.2021      Session: 2
### Päivittäinen ajankäyttö:
- Day 1: 2h 50min
- Day 2: 7h 5min
- Day 3: 8h 45min
- Day 4: 3h 30min

---
## Tausta
PHP on täysin tuntematon entuudestaan. Ainut asia mitä tiedän on se, että se on ohjelmointikieli jossakin muodossa. Lisäksi lähetin sähköpostiviestin, jossa oli lisäkysymyksiä tehtävään liittyen...

### Tehtävät asiat:
- Selvittää miten PHP toimii tai mihin perustuu.
- Esisuunnittelu
- Odottaa vastaukisa sähköpostiviestistä, että pääsee suunnittelemaan tarkemmin.
- Extra: Lisätä havainnot osioon Tasuta ja mahdolliset ongelmat.

### Havainnot:
- PHP vaikuttaa perusohjelmointi kieleltä kuten python tai C, mutta se vain tapahtuu html:n sisällä.
- Voisi olettaa, että PHP on html "laajennus". Esim sillä, että html komennot toimivat siihen niinkuin css. En siltikään osaa sanoa varmaksi ennen kokeilua...
- Muuttujat PHP:ssä määräytyvät automaattisesti 
- Toimiikohan tässä if lauseet samalla toiminalla kuin javascriptissä? == processed & === un-processed
  
---
## Environment
- Operating System: Windows 10 Pro 64-bit
- PHP Version: 8.0.2 
- Database: PostgreSQL 
- Text Editor: Visual Studio Code
- Visual Studio Code Extensions:
  
![Extensions](/images/other/Extensions.jpg)
 

---
## Esisuunnittelu
### Toiminta:
Tässä olisi esi-concept piirrustus. Kysesisessä kuvassa näkyy miten käyttäjä pysty täyttämään lomakkeen ja painamaan submit nappia. Function osa taas näyttää mitä kaikkea tapahtuu PHP tasolla kun nappia painetaan. Oletus on, että meillä olisi mahdollisuus käytää tätä kysesitä sivua missä vain ja tallennus tapahtuu serverille ip:n kautta...

Todellisuudessa tässä "demossa" sivusto ja database ovat locaaleja eli if statement errorit tuskin ovat kovin relevantteja... Databaseen saatetaan joutua laitamaan id:t ja timestampit, mutta tämän ei pitäisi hidastaa etenemistä.

![esi-concept](images/concept/site-concept.jpg)

### Näkymä:
Sivun loppunäkymä/styling tavoite olisi tällainen:

![design1](images/design/Example%20Design.jpg)

tai 

![design2](images/design/Example%20Design2.jpg)

Lopputulos tuskin on samanlainen, mutta samoihin elementeihin pyritään...

### Database:
Ohjeistuksessa ei ollut mitään mainintaa mitä databasea pitäisi käyttää tai millä yhteyksillä.
-->Käytetään SQL Databasea, koska tieto on lomakkeesta. Tiedon ei tarvitse olla "vapaamuotoista"

### Mahdolliset ongelmat
- syntax päänsärky
- host metodit
- järkevä form validation, jos html käyttö on kokonaan kielletty

---
## Suunitelma/Dokumentit
### Toimintatapa
Kyseisessä projektissa ei ole erillisiä servereitä php tai database palveluihin --> käytetään koneensisäisiä palvelumetodeja (localhost)

### Database 
Pienen tutkimuksen jälkeen tulin tulokseen, että käytän PostgreSQL databasea MySQL:n sijaan. 

Syyt olivat:
- MySQL on Oracle:n alaisena --> enterprice "paywall", joka voisi olla tulevaisuudessa ongelma yritykselle
- PostgreSQL on open source projecti ei tuote (ylempi pointti)
- Sisältää paljon eri advanced optioneita --> mahdollisuus kehittää tulevaisuudessa tarvittaessa

Itse rakenne tulisi olla seuraava: 

![database details](images/concept/database.jpg)

### Website Rakenne
Tulee olemaan hyvin samanlainen kuin esisuunitelmassa, toteutus prosessit jaetaan 5 vaiheeseen. Tämä mahdollistaa helpon ongelman ratkaisun, jos ongelimia tulee vastaan. 

1. Database Setup
2. form.php form creation
3. Data transfer to submitprocess.php
4. Establish connection to databse
5. Insert recieved data to database

Webform voisi luultavasti kokonaan tehdä 1 sivuun, mutta tämän tekeminen 2 sivulla on lopuunsa siistimpää koodillisesti ja yksinkertaisempaa + mahdollistaa molemille oman css tiedoston tarvittaessa.

Tässä tarkennettu concept kuva:

![concept site funcition](images/concept/website-concept.jpg)

### Use-case

![UseCase](images/concept/use-case.jpg)


### Sequence diagram

![sequence diagram](images/concept/sequence.jpg)


---
## Toteutus
### Database 
1. Avasin SQL Shellin ja loin uuden databasen komennolla: 
   
        CREATE DATABASE phptask; 
    
    ja menin databaseen: 
    
        \c phptask

2. Luodaan table:
        
        CREATE TABLE form_data( 
            id  serial PRIMARY KEY,
            first_name VARCHAR(20) NOT NULL,
            sur_name VARCHAR(20) NOT NULL,
            email_address VARCHAR(255) UNIQUE NOT NULL,
            phone_number VARCHAR(20) UNIQUE NOT NULL
        );

3. Lisätään testiksi databaseen dataa:
   
        INSERT INTO form_data(first_name,sur_name,email_address,phone_number) 
        VALUES('Rane', 'Hyvönen', 'email@domain.com', '358123456789');

Tässä kuva koko prosessista:
![databasesetup](images/toteutus/database/dbsetup.jpg)

 Nyt pitäisi olla sellaisessa vaiheessa, että jos ulkoisesti lähetetään dataa oikeilla nimillä oikeaan osoitteeseen, niin sen pitäisi tallentua...

### form.php
Form osuus voitaisiin mahdollisesti tehdä "php koodilla", mutta html formin käyttäminen on yksinkertaisempi ja parempi ratkaisu. php koodiin voidaan käyttää html:ää eli koodi on yhä php tiedosto vaikka siinä ei itsesään käytettäisi <?php?> ominaisuuksia. Tämän lisäksi html antaa form muodolle yksinkertaisen validate email ominaisuuden(täytyy sisältää @ ja . osuuden oikeassa järjestyksessä). 

Tämä ei ole turvallisin, mutta jos sitä etsittäisiin niin lähettäisiin email sinne ositteeseen satunnaisen varmennus-sarjan kanssa. Sitten kun oikea koodi olisi laitettu niin sitten tapahtuisi data rekisteröinti.

Databasen peruusteella tarvitaan merkkirajoitteilla etunimi, sukunimi, puhelin-numero ja sähköpostiosoite.
Eli tarvitaan tekstiä ilmoittamaan osat ja jokaiselle input osat ja lähetys nappi.

        <section class="web-form">
            <!--Form section without autocomplete-->
            <form action="submitprocess.php" method="POST" autocomplete="off">
                <label for="firstName">First Name</label>
                <input required maxlength="20" name="firstName" type="text" id="firstName" placeholder="Your first name"/>
                <label for="surname">Surname</label>
                <input required maxlength="20" name="surname" type="text" id="surname" placeholder="Your surname"/>
                <label for="phone">Phone Number</label>
                <input required maxlength="20" name="phone" type="text" id="phone" placeholder="Your phone number"/>
                <label for="email">Email</label>
                <input required maxlength="255" name="email" type="email" id="email" placeholder="Your email address"/>
                <button type="submit">Submit</button>
            </form>
        </section>

Tällä saamme yksinkertaisen form lomakkeen, joka avaa submitprocess.php sivun kun painetaan submit nappia ja laitaa input parametrit eteenpäin.
![Basic form](images/toteutus/form/basic.jpg)

### submitprocess.php
1. Ensimäinen aisa mikä pitää tehdä on varmistaa, että data tulee perille submitprocess.php sivustoon. Tämän testataan koodilla:
    
        <?php    
        //Get form data
        $firstName = $_POST['firstName'];
        $surname = $_POST['surname'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];

        //Print recieved data
        echo $firstName, $surname, $phone, $email;
        ?>
Testauksen tulos oli mitä voi odottaa:
![sent data](images/toteutus/submit/form-input.jpg)
![recieved data](images/toteutus/submit/form-output.jpg)

2. Nyt kun tiedetään, että data tulee perille niin pitää katsoa miten voimme käyttää sitä "" sisällä. Korvaa echo edellisestä koodista tällä:
    
        //Print-test recieved data inside of "" 
        $query = "($firstName) heloo. This is your friend $firstName and other '$firstName'";
        echo $query;

    Tällä saamme selville miten $muuttujat reagoivat "". Tulos:
    ![text test](images/toteutus/submit/text-test.jpg)
    Tämän perusteella $muuttuja tunnistetaan "" sisällä ja se ei tarvitse extra merkejä siihen.

3. Nyt koitamme yhdistää databaseen ja displaytää eror message jos se ei onnistu:
   
   **Tässä vaiheessa iski extension ongelma. Tarkemmat tiedot löydät Ongelmat Kohdasta ja miten asia ratkaistiin**

   Tässä on connection koodi:

        <?php
        // Create connection
        $dbConnection = pg_connect("host=localhost dbname=phptask port=5432 user=postgres password=chM6E89LDRX&B#HN")
        or die('Could not connect: ' . pg_last_error());
        //Close connection
        pg_close($dbConnection);
        //Display Success message
        echo "Success. User was added to database\n";
        ?>

    Tulos:
    ![connection test](images/toteutus/submit/connection-success.jpg)

4. Nyt kun tiedämme, että yhteys toimii voimme siirtyä data insert osioon.
   Data insrt tapahtuu koodilla: 

        <!--Send data process-->
        <?php    
        //Get form.php data
        $firstName = $_POST['firstName'];
        $surname = $_POST['surname'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];

        // Create connection
        $dbConnection = pg_connect("host=localhost dbname=phptask port=5432 user=postgres password=chM6E89LDRX&B#HN")
        or die('Could not connect: ' . pg_last_error());

        //Data insert command
        $sendData = "INSERT INTO form_data(first_name,sur_name,email_address,phone_number) VALUES('$firstName', '$surname', '$email', '$phone');";

        //Issue Query sendData
        $queryResult = pg_query($dbConnection, $sendData);

        //Query did not work. Display error message & exit
        if (!$queryResult) {
            echo "An error occurred.\n";
            exit;
        }

        //Query was a success. Close connection & Display success message
        else{
            pg_close($dbConnection);
            echo "Success. User was added to database.";
        }
        ?>

Process:
1. ![form](images/toteutus/submit/form-datainsert.jpg)
2. ![success message](images/toteutus/submit/success-datainsert.jpg)
3. Confirm Process 
   
   ![Database Changes](images/toteutus/submit/database-confirm.jpg)

Nyt meillä on toimiva webform, joka lähettää dataa databaseen.

---
## Ongelmat
### PHP ei tunnistanut php PostgreSQL osion komentoja
    Testaus koodi: 
    <?php 
    // Create connection
    $dbConnection = pg_connect("host=localhost dbname=postgres port=5432 user=postgres password=chM6E89LDRX&B#HN")
    or die('Could not connect: ' . pg_last_error());

    //Close connection
    pg_close($dbConnection);
    echo "success\n";
    ?>
Ratkaisu prosessi:
1. Selvittää miksi tämä prosessi ei toimi --> menin php pg_connect osioon(https://www.php.net/manual/en/function.pg-connect.php) ja huomasin että artikkelin juuri on "database extensions". --> tästä päättelin, että joko minulla ei ole extensionia käytössä tai sitä ei ole suoraan php asennuksessa(luultavin sillähetkellä)
   ![database extension](images/php%20problem/huom1.jpg)

2. Nyt kun tiesin, että tarvitsen tämän extensionin kysymys kuuluu miten? Katsoin php:n antamat ohjeet:
   ![instructions](images/php%20problem/instru.jpg)
   
   Näistä ei ollut kovin mitään hyötyä näin vilkaisemalla. Joten päätin tehdä Googlailua miten PostgreSQL extension saataisiin toimimaan. Yhteinen idea kaikissa oli, että php.ini tiedostoon pitää lisätä linja koodia. 
3. Nyt haasteena oli löytää tämä kyseinen tiedosto. Minun yllätykseni tätä tiedostoa ei löytynyt. Oli vain php.ini-development & php.ini-production. 
4. Päätin tutkia näitten tiedostojen sisältöä, jos sieltä löytyisi uutta tietoa.
   Sieltä löytyi seuraavat kuvaukset:
   ![description](images/php%20problem/php-ini.jpg)
   
   Tästä sain selville että molemmat ovat ini tiedostoja. --> lisäsin jonkinlaisen extension kohdan koodiin, koska error server puolella on suuri askel eteenpäin. 
   Samalla koostin kuvan, että jossakin on config file joka määräisi onko dev vai production versio käytössä.
5. Tämän tiedon saadakseni päätin katsoa php tiedot koodilla:
        
        <?php
        phpinfo();
        ?>

    ![path](images/php%20problem/huom2.jpg)
    
    Huomiot: 
    1. ini tiedostolle ei ole reittiä tai tiedostoa ei ole olemassa(Luultavin). 
    2. Mitään postgreSQL osiota ei ollut tiedoissa --> ei käytössä.
   
6. Päätin kokeilla muuttaa ini-developnment tiedoston päätteen pelkäiksi ini:ksi. 
   
   ![namechange](images/php%20problem/files.jpg)

   Tämän jälkeen tuli heti tuloksia serverillä sekä php infossa:

    ![server reaction](images/php%20problem/error-message-path.jpg)
    ![php info](images/php%20problem/phpwithpath.jpg)
    
    Nyt huomaa, että php.ini config file toimii niinkuin pitääkin. Ainut asia mikä puuttuu on extensinin oikea nimi tai itse tiedosto. 

7. Menin sokkona katsomaan php\ext kansion sisältöä jos sieltä löytyisi valmiina postgreSQL tiedosto ja yllätykseni "php_pgsql.dll" tiedosto.
8.  Menin takaisin php.ini tiedostoon ja vaihoin laittamani koodin joka aiheutti errorin seuraavaan: "extension=php_pgsql.dll" ja kokeilin serverin käynnistämistä uudelleen

    Nyt PHP  infossa näkyy laajennus, server ei antanut error messageja ja alkuperäinen koodi antaa success ilmoituksen
    ![pgsql section](images/php%20problem/pgsql.jpg)
    
---
## Loppusanat | Lopputulos

Itse sanoisin, että projekti onnistui loppuunsa hyvin. Dokumentointi olisi voinut olla siistimpää ja ehkä liian step by step tyylinen. Lopputulos on sellainen, joka mielyttää, ei ole omasta mielestä kuhan jotenkin tehty ja mikä tärkeintä... oli hauska tehdä.

Inspiraatio kuva: 

![design2](images/design/Example%20Design2.jpg)

Lopputulos: 

![final form](images/done/form.jpg)
![final message](images/done/success.jpg)
