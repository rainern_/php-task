# Code Review
Käyn läpi kirjoitetun koodin ja taustaa valintoihini...

## Database

![database details](/images/concept/database.jpg)

Jokaisessa paitsi id:ssä on merrki rajoitukset, muistin/tallenustilan vuoksi.

id: Ideana on käyttää id:tä PRIMARY KEY:nä ilman, että sinun täytyy ohjelmassa lisätä tämä itse. Saamme sen käyttämällä serial typeä ja ilman NOT NULL vaatimusta.

Nimet: Pyydämme käyttäjältä etunimeä ja sukunimeä, joten 20 merkin pituisuus molempiin pitäisi rittää. NOT NULL on käytössä.

Sähköposti osoitteet: Itsellä ei ole 100% varmuutta miten pitkiä sähköpostiosoitteet voi olla, mutta käytänössä minkään ei pitäisi ylittää 255 merkkiä... 

Puhelin numerot: Olen laittanut 20 merkin rajan, koska jos käyttäjä laitaa +123 15merkkiä niin sen pitäisi mennä juuri ja juuri. Ei pitäisi käytänössä olla ongelma...

---
## form.php
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">

Lyhennetysti tämä tarkoittaa, että tiedoston tyyppi on html ja sen kieli on englanti. Sivuston käyttämät merkit ovat UTF-8 unicode standardin mukaisia.

        <meta http-equiv="X-UA-Compatible" content="IE=edge">

Tämä tag antaa sinun valita millä internet explorer versiolla sivu renderitetään, jos siis käytetään sitä. Koska IE on jo aika unohdettu kun edge tuli microsoftilta ja chrome googlelta, on ollut kiistaa tarvitaanko tätä tagia ollenkaan ja onko siitä jopa haitaa...

        <meta name="viewport" content="width=device-width, initial-scale=1.0">

Tag antaa selaimelle ohjeet miten hallita sivuston sisältöä --> responsive design

        <title>Web Form | Amazing Company</title>

Sivuston title... itsestään selvä. Tässä tapauksessa sisältää sivun missä olet ja kenen sivustolla(mahdollisuus leikkiä markenoinnilla)

        <link rel="shortcut icon" href="/project/form-content/amazing-company-favicon.png"/>

Linkataan Favicon sivustoon... branding

        <!--Allow Crawlers (for google SEO)-->
        <meta name="robots" content="index,follow">

Tämän kommentti selittää tämän aika hyvin... Käytetään "index,follow" jos halutaan, että sivu löytyy helpommin googlen hausta ja "noindex,nofollow" jos ei... Asiasta enemän tietoa: https://developers.google.com/search/docs/advanced/crawling/overview-google-crawlers

        <meta name="description" content="Web form of amazing company that registers people in their database.">

Sivuston kuvaus. Hyödylinen käyttäjille ja SEO/marketointiin...

        <link rel="stylesheet" href="form-style.css">
    </head>

Linkitetään css stylesheet tiedostoon...

    <body>
        <div class="background">

Itse sivuston "materiaali" alkaa body tagista ja aloitetaan luomalla oiso, jonka tarkoitus on olla background meidäin formille. Tässä tapauksessa se on valkoinen osuus formin takana, jonka sisällä kaikki on paitsi suuniteltu kuva tausta.

        <img src="/project/form-content/amazing-company-logo.png" alt="Amazing Company Logo">

Lisätään ekana taustaan yhtiön logo. src=missä logo on ja alt=teksti, joka näkyy jos kuva ei renderöi tai ei löydy. Tämä on myös osa SEO:ta

        <section class="web-form">
Luodaaan uusi osio nimeltään web-form. Tarkoituksena on saada yhtienen muokattava css osa formille(inherit parent css). Tätä ei välttämättä tarvittaisi, jos on vain 1 form mitä muokata.

            <!--Form section without autocomplete-->
            <form action="submitprocess.php" method="POST" autocomplete="off">

Luodaan form, jossa ei ole autocomplete ominaisuutta. Tämä ominaisuus ei ole pakko, mutta jos kyse on yhdestä koneesta jolla laitetaan eri ihmisten tietoja --> complete lista tulee vain tielle.

Action on minne tiedot lähetetään/käytetään & POST on lähetys tapa. Valitsin POST tavan koska:
- lähetetty data ei näy address barissa eli ei history ongelmia
- POSTilla ei ole koko rajotuksia
- form tietoja ei voida bookmarkata

                <label for="firstName">First Name</label>
                <input required maxlength="20" name="firstName" type="text" id="firstName" placeholder="Your first name"/>


Minulla on 2 valintaa miten olisin voinut suorittaa tämän vaiheen.
1. Olisin luonut pelkästään input fieldit ja placeholderiellä laittanut mitä siihen pitää laittaa.
2. Tai laitaa niihin tekstit eteen tai ylös mitä field haluaa. 
Valitsin vaihtoehdon 2, koska liian usein täytän lomakkeen ja sen jälkeen tarkistan että kaikki on ok. Sitten epäillys tulee, että oliko tämä oikea field. Tämä on loppuunsa personal preference...

Valitsin tekstin käyttöön labelin, koska se on parempi mobiili käyttäjille: Voit laitaa labeliin for ominaisuuden --> paina label tagia ja se antaa sinun kirjoittaa siihen tarkoitettuun input fieldiin. Tämä on etu mobiili käytössä, koska on enempi aluetta mistä painaa --> parempi jos käsi tärisee tai on vitsillisesti "nakkisormet"

input on ihan perus input field, joka ominaisuus on teksti ja sisältää ohje tekstin ja vaatimuksen, että kenttä ei voi olla tyhjä ja annettu teksti ei ylitä 20 merkkiä (database rajoitus).

                <label for="surname">Surname</label>
                <input required maxlength="20" name="surname" type="text" id="surname" placeholder="Your surname"/>
                <label for="phone">Phone Number</label>
                <input required maxlength="20" name="phone" type="text" id="phone" placeholder="Your phone number"/>
                <label for="email">Email</label>
                <input required maxlength="255" name="email" type="email" id="email" placeholder="Your email address"/>
                
Hyvinpaljon samanlaista kuin edellinen. Tässä on type selitykset, jos tarvitsee:

Puhelinnumero: Text sen takia, jos halutaan laitaa esim + merkki eteen niin voidaan ja numerot eivät häviä kun ei pidä. esim puhelin numero 0441234123 tallentuisi 441234123 jos ne tallenettaisiin numeroina

Sähköposti: Käyttää email typeä, koska sitten siinä on html:n oma validate systeemi. Mainitsin tästä main README tiedostossa. Tähän voitaisiin lisätä oma validate metodi, mutta se ei ole aina sen arvoista. Tämä riippuu sovelluksen taustasta. Lisäksi send email validate tai email filter ovat hyviä, jos pitää olla oikeita oikeita sähköpostiosoitteita.


                    <button type="submit">Submit</button>
                </form>
            </section>
        </div>
    </body>
    </html>

Lopuksi lisätään yksinkertainen nappi formin hähetystä varten ja suljetaan form & muut osuudet.

---
## submitprocess.php

    <body>
        <section class="result">

Sama asia kuin formin background osuus. Halutaan background osa, jolla voidaan ohjata myös php osaa.

            <!--Send data process-->
            <?php   
            //Get form data
            $firstName = $_POST['firstName'];
            $surname = $_POST['surname'];
            $phone = $_POST['phone'];
            $email = $_POST['email'];

Kommentti osa kertoo kaiken....

            // Create connection
            $dbConnection = pg_connect("host=localhost dbname=phptask port=5432 user=postgres password=chM6E89LDRX&B#HN")
            or die('Could not connect: ' . pg_last_error());

Yritetään yhdistää databaseen pg_connect komenolla. Antaa errorin ja sulkeutuu jos yhteys ei onnistu.

            //Data insert command
            $sendData = "INSERT INTO form_data(first_name,sur_name,email_address,phone_number) VALUES('$firstName', '$surname', '$email', '$phone');";

Muutujaksi muutettu komento miten siirretään dataa databaseen. Tämä ei ole pakollista, mutta minun mielestä on siistimpi ja paremman näköinen näin.

            //Issue Query sendData
            $queryResult = pg_query($dbConnection, $sendData);

Aloitetaan databaseen tiedonsiirto pg_query komenolla, jonka sisään määritetään minne yhdistetään, jos ei ole valmista yhteyttä eli dbConnection ja mikä command eli sendData. Jos query ei onnistu niin queryresult=false.

            //Query did not work. Display error message & exit
            if (!$queryResult) {
                echo "An error occurred.\n";
                exit;
            }

Äsköiseen viitaten: jos qeuryResult=false --> ilmoitetaan error ja suljetaan process

            //Query was a success. Close connection & Display Success message
            else{
                pg_close($dbConnection);
                echo "<p>Success. $firstName was added to database.</p>";
            }
            ?>

Jos query onnistui niin suljetaan yhteys databaseen ja ilmoitetaan käyttäjälle: kaikki ok ja data lisättiin databaseen.

            <!--Back button to form-->
            <a href="form.php">
                <button type="back">Back</button>
            </a>
        </section>
    </body>
    </html>

Lopuksi lisätään result taustaan nappi jolla pääsemme takaisin formille. Nappiin olisi voinut käyttää eri tapoja, mutta päätin käyttää ankkuri tageja (vai mikä onkaan suomeksi <.< )

---
## form-style.css

    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

Tällä asetamme hävitämme turhat marginaalit ja paddingit lisäksi annamme elementeille border-box ominaisuuden --> niiden koko on content, padding ja border

    html{
        font-size: 62.5%;
        background-image: url(./form-content/background.jpg); /*Photo by Simon Berger from Pexels*/
        background-position: center center;
        background-repeat: no-repeat;
        background-attachment: fixed;
        background-size: cover;

        background-color: #464646;
        padding-top: 10%;
        padding-bottom: 5%;
        text-align: center;
    }

Font-size on asetetu 62.5%, koska haluan käyttää rem kokoa em:in tai px:n sijaan. Syynä tähän on, että rem scaalautuu selain text sizen mukaan ja sen koko ei kertaudu joka kerta kuin em. Eli käytänössä rem on enemmän reaktiivisempi ja sen käyttö on yhtä helppoa kuin px jos asettaa koon 62.5% (1.6rem=16px)

Tässä asetamme formille ja submitprocessille responsive taustan. Koska tausta on kuva sinne on lisätty latauksen ajaksi taustaväri.

Padding on taas pieni gap creator, joka scaalaa ruudun mukaan --> toimii sopivasti mobiililaiteilla 

Text-align on vain ominaisuus, jonka haluan jokaiseen elementtiin defaultina jos mahdollista tässä projektissa.

    .background{
        background-color: white;
        width: max-content;
        height: max-content;
        margin: auto;
        padding: 2rem 3rem;
}

Tämä on formin valkoinen tausta. idea on, että se leviää suurimman form contentin perusteella (saadaan tasainen "palikka") ja liviää pikkaisen sen jälkeen antaakseen muotoa. Lisäksi annetaan myös auto marginin form elementeille.

    .web-form label{
        margin-top: 2rem;
        font-size: 2rem;
        display: block;
    }

    .web-form input{
        font-size: 2.5rem;
        display: block;
    }

Näillä säädämme formin osat erillään toisista ja muutamme tekstien kokoja.

    .result button,
    .web-form button{
        margin-top: 1.7rem;
        padding: 2rem 4rem;
        border: 0.3rem solid rgb(46, 148, 77);
        color: white;
        font-size: 2.4rem;
        border-radius: 1rem;
        background: rgb(46, 148, 77);
    }

Tällä teemme formille sekä submitprocess.php filen back napille samat muutokset. Nämä ovat vain kaunistuksia ja margin top osaan että se ei ole lomakkeessa kiinni...

    .result button:hover,
    .web-form button:hover{
        background: rgb(25, 109, 50);
        border: 0.3rem solid rgb(25, 109, 50);
    }

    .result button:focus,
    .web-form button:focus{
        background: firebrick;
        border: 0.3rem solid firebrick;
    }

Tässä on samoille napeille hover muutos --> muuttuu eri sävyiseksi vihreäksi
ja kun nappia painetaan se muuttuu punaiseksi.

    /*Inherit from * & html*/
    .result{
        font-size: 5rem;
        background-color: white;
        margin-top: 8rem;
        padding: 1rem 0rem 2rem 0rem;
    }

Tämä osuus on taas submitprocess.php taustaksi ja koska taustassa on on vain 1 rivi tekstiä niin sen muutokset voidaan tehdä tässä
