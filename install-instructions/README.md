# Asennus Dokumentaatio

## PHP Asennus:
1. Mene sivulle https://windows.php.net/download
2. Valitse koneellesi uusin sopiva versio (itsellä se on VS16 x64 Thread Safe V:8.0.2) ja lataa sen zip tiedosto
3. Extract zip filen tiedostot C: lokatioon nimellä php
4. Kirjoita enviro windows hakuun ja pitäisi tulla option "Edit the system environment variables" ja paina sitä <mark>Huomio: ei löydy luultavasti suomenkielisellä windowsilla...</mark>

![search](../images/php-install/search.jpg)

5. Paina Environment Varables... nappia
   
![enviroment](../images/php-install/envir1.jpg)

6. Valitse path, että se on highlightetty ja paina edit

![edit](../images/php-install/envir2.jpg)

7. Paina new ja kirjoita siihen "C:\php" jos seurasit ohjetta... ja sen jälkeen ok
   
![add](../images/php-install/envir3.jpg)

8. Jobs done. Voit tarkistaa asennuksen menemällä cmd:hen tai powershelliin ja kirjoitat "php -v" ja sen pitäisi antaa php versio ja tekijänoikeudet

![php versio](../images/php-install/phpv.jpg)

---
## PHP Local Hosting:
1. Powershellissä menin projekti/git kansioon & aloitan local host prosessin komenolla:"php -S localhost:4000"

    php = php komento 

    -S = run with built-in web server 

    localhost:4000 = osoite:portti


![](../images/localhost/php%20setup.jpg)

2. Mene osoitteeseen http://localhost:4000/project/form.php nähdäksesi projektin (kuva on demo)

![live site](../images/localhost/livesite.jpg)

Kun yhdistät sivulle niin powershellissä pitäisi näkyä yhteydet:

![connection](../images/localhost/php%20connected.jpg) 

"server" sammutus tapahtuu "Ctrl + C"

## PostgreSQL Asennus:
1. Menin sivulle https://www.enterprisedb.com/downloads/postgres-postgresql-downloads ja painoin "download" win 64bit versioon
2. Seurasin heidän omaa install guidea, joka löytyy osioteesta: https://www.enterprisedb.com/edb-docs/d/postgresql/installation-getting-started/installation-guide/13.0/invoking_the_graphical_installer.html
Ainoat erot olivat, minun oma custom salasana ja asennus dirrectory on "C:\PostgreSQL"
<mark>!Huom! en asentanut mitään "stackista"</mark>

3. Hallitse databasea joko SQL Shellin kautta tai pgAdmin4 kautta. 
