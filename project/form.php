<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Web Form | Amazing Company</title>
    <link rel="shortcut icon" href="/project/form-content/amazing-company-favicon.png"/>
    <!--Allow Crawlers (for google SEO)-->
    <meta name="robots" content="index,follow">
    <meta name="description" content="Web form of amazing company that registers people in their database.">
    <link rel="stylesheet" href="form-style.css">
</head>
<body>
    <div class="background">
        <img src="/project/form-content/amazing-company-logo.png" alt="Amazing Company Logo">
        <section class="web-form">
            <!--Form section without autocomplete-->
            <form action="submitprocess.php" method="POST" autocomplete="off">
                <label for="firstName">First Name</label>
                <input required maxlength="20" name="firstName" type="text" id="firstName" placeholder="Your first name"/>
                <label for="surname">Surname</label>
                <input required maxlength="20" name="surname" type="text" id="surname" placeholder="Your surname"/>
                <label for="phone">Phone Number</label>
                <input required maxlength="20" name="phone" type="text" id="phone" placeholder="Your phone number"/>
                <label for="email">Email</label>
                <input required maxlength="255" name="email" type="email" id="email" placeholder="Your email address"/>
                <button type="submit">Submit</button>
            </form>
        </section>
    </div>
</body>
</html>