<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Thank You | Amazing Company</title>
    <link rel="shortcut icon" href="/project/form-content/amazing-company-favicon.png"/>
    <!--Allow Crawlers (for google SEO)-->
    <meta name="robots" content="noindex, nofollow">
    <meta name="description" content="Amazing companys form process page.">
    <link rel="stylesheet" href="form-style.css">
</head>

<body>
    <section class="result">
        <!--Send data process-->
        <?php   
        //Get form data
        $firstName = $_POST['firstName'];
        $surname = $_POST['surname'];
        $phone = $_POST['phone'];
        $email = $_POST['email'];

        // Create connection
        $dbConnection = pg_connect("host=localhost dbname=phptask port=5432 user=postgres password=chM6E89LDRX&B#HN")
        or die('Could not connect: ' . pg_last_error());

        //Data insert command
        $sendData = "INSERT INTO form_data(first_name,sur_name,email_address,phone_number) VALUES('$firstName', '$surname', '$email', '$phone');";

        //Issue Query sendData
        $queryResult = pg_query($dbConnection, $sendData);

        //Query did not work. Display error message & exit
        if (!$queryResult) {
            echo "An error occurred.\n";
            exit;
        }

        //Query was a success. Close connection & Display Success message
        else{
            pg_close($dbConnection);
            echo "<p>Success. $firstName was added to database.</p>";
        }
        ?>

        <!--Back button to form-->
        <a href="form.php">
            <button type="back">Back</button>
        </a>
    </section>
</body>
</html>